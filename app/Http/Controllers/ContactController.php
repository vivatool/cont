<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api\GoogleSheet;

class ContactController extends Controller
{
	private $rules = [
		'contacts' => 'required|array',
		'contacts.*.tel' => 'required|string',
		'contacts.*.name' => 'required|string',
		'contacts.*.email' => 'required|string|email',
		'city' => ['required', 'string', 'regex:/^od$|^kv$|^kh$|^dp$|^vn$/']
	];


	public function store(Request $request)
	{
		$this->validate($request, $this->rules);
		$contacts = $request->all();

		foreach ($contacts['contacts'] as &$contact) {
			$contact['tel'] = str_replace(['(', ')', '-'], '', $contact['tel']);
			GoogleSheet::insert($contact, $contacts['city']);
		}

		return response()->json(["status" => "ok"]);
	}
}
